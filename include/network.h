#pragma once

#include <sys/types.h>

ssize_t server(int fd, const char *data);
int sendListMSG(int fd);
int sendField(int fd);
