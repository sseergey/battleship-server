#pragma once

#include "dblist.h"
#include "event.h"


struct dblist_t* init_queue(void);
void release_queue(struct dblist_t* queue);

void put_to_queue(struct dblist_t* queue, struct event_t* item);
struct event_t* get_from_queue(struct dblist_t* queue);
