#pragma once

#include <stdbool.h>

struct event_t;
struct data_t;

void supervisor(const struct event_t* event);
void client_supervisor(const struct event_t* event);
bool checkForNameGlobbing(const struct data_t* data, const struct data_t* wait_data);
void want_supervisor(const struct event_t* event);
void agree_supervisor(const struct event_t* event);
void gen_event_invite(int fd, const char* data);
