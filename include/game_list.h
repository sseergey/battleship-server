#pragma once

#include "dblist.h"

struct player_t {
    int fd;
    int current_ship;
    char field[10][10];
    char enemyField[10][10];
    int killedDecks;
    bool agree;
    bool req;
};

struct game_t {
    struct dblist_t dblist;

    struct player_t player1;
    struct player_t player2;

    int ship[10];
    bool game_started;
};

struct dblist_t* init_game_list(void);
void release_game_list(struct dblist_t* list);

void put_to_game_list(struct dblist_t* list, struct game_t* item);
struct game_t* get_from_game_list(struct dblist_t* list, int fd);
void remove_from_game_list(struct dblist_t* list, int fd);
