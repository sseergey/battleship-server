#pragma once

#include "dblist.h"

struct event_t {
    struct dblist_t dblist;
    char data[80]; // want, create, agree
    int fd; // fd=0 msg to superviser
    int fromFd;
    int enemyFd;
    bool agree;
};
