#pragma once

#include "dblist.h"
#include "enemy_list.h"

struct data_t {
    struct dblist_t dblist;
    char buf[80];
    int state;
    int fd;
    int enemy_fd;
    char enemy_name[60];
    char name[60];

//    struct dblist_t* enemy_list;
};


struct dblist_t* init_client_list(void);
void release_client_list(struct dblist_t* list);

void put_to_client_list(struct dblist_t* list, struct data_t* item);
struct data_t* get_from_client_list(struct dblist_t* list, int fd);
void remove_from_client_list(struct dblist_t* list, int fd);
