#pragma once

#include "event_queue.h"
#include "client_list.h"
#include "enemy_list.h"
#include "game_list.h"

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>


enum client_states {
    CLOSED = 0,
    HANDSHAKE,
    ACQ_RECV,
    CHOOSE_RIVAL_RECV,
    WAIT_GAME_OFFER,
    WAIT_GAME_STATUS,
    WAIT_SV_EVENT,
    GAME_START,
    GAME_ACTION,
    PLACE_SHIPS
};


enum misc_consts {
    MESSAGE_BUFFER_SIZE = 512,
    MESSAGE_MAX_SIZE = 80
};


struct client_list {
    struct dblist_t dblist;
    struct data_t data;
};


struct queue_event {
    struct dblist_t dblist;
    struct event_t event;
};


extern struct dblist_t* client_list;
extern struct dblist_t* waiting_players_list;
extern struct dblist_t* game_list;

void put_to_event_queue(struct event_t* item);
void put_to_kill_list(int fd);
void put_to_replace_list(int fd, const char *name);
