#pragma once

#include <stdbool.h>

struct dblist_t {
    struct dblist_t* next;
    struct dblist_t* prev;
};

void init_dblist(struct dblist_t* dblist);

void release_dblist(struct dblist_t* dblist);

struct dblist_t* first_dblist_element(const struct dblist_t* dblist);

struct dblist_t* last_dblist_element(const struct dblist_t* dblist);

void add_to_dblist(struct dblist_t* dblist, struct dblist_t* elem);

void remove_from_dblist(struct dblist_t* elem);

bool is_dblist_empty(const struct dblist_t* dblist);

bool is_dbvalid(const struct dblist_t* dblist, const struct dblist_t* elem);
