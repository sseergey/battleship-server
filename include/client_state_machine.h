#pragma once
#include <stdbool.h>

struct data_t;
struct event_t;

void client(const char *buf, struct data_t* data, const struct event_t* recv_event);

int handshake(const char *recv_msg, int fd);
int acqRecv(const char *recv_msg, int fd, struct data_t *data);
int chooseRivalRecv(const char *recv_msg, struct data_t* data);
int waitGameOffer(const char *recv_msg, struct data_t* data, const struct event_t *recv_event);
int waitGameStatus(const char *recv_msg, int fd);
int waitSvEvent(const char *recv_msg, int fd);
int placeShip(const char *recv_msg, int fd);
int gameAction(const char *recv_msg, int fd);
int closed(void);

bool assembleString(char* recv_msg, struct data_t* data, const char* buf);
bool nameIsOk(int fd, const char* name);
bool isShipOk(int x, int y, char dir, int length, int fd);
bool isShipHere(char point);
bool isInBoardRange(int x);


int attack(int x, int y, int fd);
int fireMSG(int fd, const char *recv_msg);
int exitMSG(int fd);

struct event_t* gen_event_ACCEPT(int fd);
struct event_t* gen_event_DECLINED(int fd);
struct event_t* gen_event_WANT(int fd);
void kill_client(int fd);
