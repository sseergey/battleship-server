#pragma once

#include "dblist.h"

struct enemy_t {
    struct dblist_t dblist;
    int fd;
    char name[80];
};


struct dblist_t* init_enemy_list(void);
void release_enemy_list(struct dblist_t* list);

void put_to_enemy_list(struct dblist_t* list, struct enemy_t* item);
struct enemy_t* get_from_enemy_list(struct dblist_t* list, int fd);
void remove_from_enemy_list(struct dblist_t* list, int fd);
