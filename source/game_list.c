#include "game_list.h"
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

struct dblist_t* init_game_list(void) {
    struct dblist_t* list = (struct dblist_t* )malloc(sizeof(struct game_t));
    init_dblist(list);
    return list;
}


void release_game_list(struct dblist_t* list) {
    assert(list);
    release_dblist(list);
    free(list);
}


void put_to_game_list(struct dblist_t* list, struct game_t* item) {
    assert(list);
    assert(item);
    struct dblist_t* last_item = last_dblist_element(list);
    add_to_dblist(last_item, (struct dblist_t* )item);
}


struct game_t* get_from_game_list(struct dblist_t* list, int fd) {
    assert(list);
    struct game_t* item = (struct game_t* )first_dblist_element(list);
    struct game_t* start = (struct game_t* )list;

    while(item != start) {
        if (item->player1.fd == fd || item->player2.fd == fd) {
            return item;
        }
        item = (struct game_t* )(((struct dblist_t* )item)->next);
    }

    return NULL;
}


void remove_from_game_list(struct dblist_t* list, int fd) {
    assert(list);
    struct game_t* item = get_from_game_list(list, fd);
    if (item != NULL)
        remove_from_dblist((struct dblist_t* )item);
}
