#include "client_list.h"
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

struct dblist_t* init_client_list(void) {
    struct dblist_t* list = (struct dblist_t* )malloc(sizeof(struct data_t));
    init_dblist(list);
    return list;
}


void release_client_list(struct dblist_t* list) {
    assert(list);
    release_dblist(list);
    free(list);
}


void put_to_client_list(struct dblist_t* list, struct data_t* item) {
    assert(list);
    assert(item);
    struct dblist_t* last_item = last_dblist_element(list);
    add_to_dblist(last_item, (struct dblist_t* )item);
}


struct data_t* get_from_client_list(struct dblist_t* list, int fd) {
    assert(list);
    struct data_t* item = (struct data_t* )first_dblist_element(list);
    struct data_t* start = (struct data_t* )list;

    while(item != start) {
        if (item->fd == fd) {
            return item;
        }
        item = (struct data_t* )(((struct dblist_t* )item)->next);
    }

    assert("fd not found" == NULL);
    return item;
}


void remove_from_client_list(struct dblist_t* list, int fd) {
    assert(list);
    struct data_t* item = get_from_client_list(list, fd);
    remove_from_dblist((struct dblist_t* )item);
}
