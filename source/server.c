#include "server.h"
#include "network.h"
#include "superviser.h"
#include "client_state_machine.h"
#include <errno.h>

enum global_consts_t {
    PORT = 2000
};


enum client_consts {
    MAX_CLIENTS = 100000
};


struct dblist_t* client_list = NULL;
struct dblist_t* waiting_players_list = NULL;
struct dblist_t* game_list = NULL;

static struct dblist_t* event_queue = NULL;
static struct dblist_t* kill_list = NULL;
static struct dblist_t* replace_list = NULL;


int main(void) {
    // FIX
    // TODO: защита от запуска 2й копии
    // TODO: сделать передачу порта в аргументах
    // инициализировать tcp сервер
    // повторять:
    //   слушать клиентов
    //   получить сообщение
    //   положить сообщение в очередь
    //   обработать очередь

    kill_list = init_enemy_list();
    replace_list = init_enemy_list();

    client_list = init_client_list();
    event_queue = init_queue();
    waiting_players_list = init_enemy_list();
    game_list = init_game_list();

    int fd = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(PORT);

    int opt = 1;
    setsockopt(fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt));
    int result = bind(fd, (struct sockaddr* )&addr, sizeof(addr));
    if (result != 0) {
        return -1;
    }


    listen(fd, MAX_CLIENTS);

    int errcode = 0;
    int client_cnt = 0;

    while(errcode == 0) {
        fd_set readfds;
        int max, retval;
        FD_ZERO(&readfds);

        // Заполняем множество дескрипторов, для select
        max = fd;
        FD_SET(fd, &readfds);

        const struct data_t* c_item = (const struct data_t* )first_dblist_element(client_list);
        const struct data_t* c_start =(const struct data_t* )client_list;
        while(c_item != c_start) {
            if (c_item->fd > max)
                max = c_item->fd;
            FD_SET(c_item->fd, &readfds);
            c_item = (const struct data_t* )((const struct dblist_t* )c_item)->next;
        }

        printf("Entering into 'select' with fd_set=%02X\n", *(const char *)&readfds);

        struct timeval tv;
        tv.tv_sec = 2;

        retval = select(max + 1, &readfds, NULL, NULL, NULL);
        if (retval < 0) {
            errcode = 1;
            int err = errno;
            printf("ERROR %d\n", err);
            // =0 time out
        }


        // Новое соединение
        if (FD_ISSET(fd, &readfds)) {
            int new_client_fd = accept(fd, NULL, NULL);

            if (new_client_fd != -1) {
                if (client_cnt < MAX_CLIENTS) {
//                    cfd[client_cnt++] = new_client_fd;
                    struct data_t* data = (struct data_t* )malloc(sizeof(struct data_t));
                    memset(data, 0, sizeof(struct data_t));
                    data->fd = new_client_fd;
                    data->enemy_fd = -1;
//                    data->enemy_list = init_enemy_list();
                    data->state = HANDSHAKE;

                    put_to_client_list(client_list, data);
                    printf("Client %d connected\n", new_client_fd);
                    server(new_client_fd, "battle ship server 1\n");
                }
                else
                    close(new_client_fd);
            }
        }

        for (const struct data_t* it = (const struct data_t *)first_dblist_element(client_list); it != (const struct data_t* )client_list;
             it = (const struct data_t* )(((const struct dblist_t* )it)->next)) {
            // Пришли данные по соединению cfd
            if (FD_ISSET(it->fd, &readfds)) {

                printf("Reading data from client %d\n", it->fd);
                ssize_t bytes;

                do {
                    char buf[MESSAGE_MAX_SIZE];
                    memset(buf, 0, sizeof(buf));

                    bytes = recv(it->fd, buf, sizeof(buf), MSG_DONTWAIT);

                    int err = errno;
                    if (bytes <= 0) {
                        // Ошибка или закрыли соединение со стороны клиента

                        struct enemy_t* enemy = (struct enemy_t* )malloc(sizeof(struct enemy_t));
                        enemy->fd = it->fd;
                        if (bytes == -1)
                            printf("ERROR %d with client %d\n", err, it->fd);
                        else
                            printf("Connection was closed by client %d\n", it->fd);

                        if (get_from_game_list(game_list, it->fd) != NULL) {
                            exitMSG(it->fd);
                        }else {
                            put_to_enemy_list(kill_list, enemy);
                        }
                    }
                    else {

                    // Ответ клиенту (создание события)
                    // Закинуть в очередь
                        struct event_t* event = (struct event_t* )malloc(sizeof(struct event_t));
                        memset(event->data, 0, sizeof(event->data));

                        strcpy(event->data, buf);
                        event->fd = it->fd;
                        put_to_queue(event_queue, event);
                    }

                } while(bytes == MESSAGE_MAX_SIZE);
            }
        }

        // Обработать очередь
        for (struct event_t* event = get_from_queue(event_queue); event != NULL; event = get_from_queue(event_queue)) {
            supervisor(event);
            free(event);
        }


        // Заменяем имя клиента на его файловый дескриптор
        struct enemy_t* change_item = (struct enemy_t* )first_dblist_element(replace_list);
        struct enemy_t* change_start = (struct enemy_t* )replace_list;

        while(change_item != change_start) {
            struct data_t* client_item = (struct data_t* )first_dblist_element(client_list);
            struct data_t* client_start = (struct data_t* )client_list;

            while(client_item != client_start) {
                if (!strcmp(client_item->enemy_name, change_item->name)) {
                    client_item->enemy_fd = change_item->fd;
                }

                client_item = (struct data_t* )(((struct dblist_t* )client_item)->next);
            }
            int change_fd = change_item->fd;
            change_item = (struct enemy_t* )(((struct dblist_t* )change_item)->next);
            remove_from_enemy_list(replace_list, change_fd);
        }

        // Убираем отключившихся или не соблюдающих протокол клиентов
        struct enemy_t* enemy = (struct enemy_t* )first_dblist_element(kill_list);
        struct enemy_t* start = (struct enemy_t* )kill_list;
        while (enemy != start) {
            int kfd = enemy->fd;
            printf("Killing client %d\n", kfd);

            remove_from_client_list(client_list, kfd);
            struct game_t* game = get_from_game_list(game_list, kfd);
            if (game != NULL) {
                const struct player_t* opp = NULL;
                if (kfd == game->player1.fd) {
                    opp = &game->player2;
                }
                else {
                    opp = &game->player1;
                }
                struct data_t* data = get_from_client_list(client_list, opp->fd);
                data->state = WAIT_GAME_OFFER;
            }
            remove_from_game_list(game_list, kfd);
            remove_from_enemy_list(waiting_players_list, kfd);
            enemy = (struct enemy_t* )(((struct dblist_t* )enemy)->next);
            remove_from_enemy_list(kill_list, kfd);

            client_cnt--;
            close(kfd);

            printf("Client %d killed\n", kfd);
        }
    }

    close(fd);
    return errcode;
}


void put_to_event_queue(struct event_t* item) {
    put_to_queue(event_queue, item);
}


void put_to_kill_list(int fd) {
    struct enemy_t* client = (struct enemy_t* )malloc(sizeof(struct enemy_t));
    client->fd = fd;
    put_to_enemy_list(kill_list, client);
}


void put_to_replace_list(int fd, const char* name) {
   struct enemy_t* client = (struct enemy_t* )malloc(sizeof(struct enemy_t));
   client->fd = fd;
   memset(client->name, 0, sizeof(client->name));
   strcpy(client->name, name);

   put_to_enemy_list(replace_list, client);
}
