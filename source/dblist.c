#include "dblist.h"
#include <stdlib.h>
#include <assert.h>

void init_dblist(struct dblist_t* dblist) {
    assert(dblist);
    dblist->next = dblist;
    dblist->prev = dblist;
}

void release_dblist(struct dblist_t* dblist) {
    assert(dblist);
    while(!is_dblist_empty(dblist)) {
        struct dblist_t* next = dblist->next;
        remove_from_dblist(next);
        free(next);
    }
}


struct dblist_t* first_dblist_element(const struct dblist_t* dblist) {
    assert(dblist);
    return dblist->next;
}


struct dblist_t* last_dblist_element(const struct dblist_t* dblist) {
    assert(dblist);
    return dblist->prev;
}


void add_to_dblist(struct dblist_t* dblist, struct dblist_t* elem) {
    assert(dblist);
    assert(elem);
    elem->next = dblist->next;
    elem->prev = dblist;
    dblist->next->prev = elem;
    dblist->next = elem;
}


void remove_from_dblist(struct dblist_t* elem) {
    assert(elem);
    elem->prev->next = elem->next;
    elem->next->prev = elem->prev;
}


bool is_dblist_empty(const struct dblist_t* dblist) {
    assert(dblist);
    return dblist == dblist->next;
}


bool is_dbvalid(const struct dblist_t* dblist, const struct dblist_t* elem) {
    assert(dblist);
    assert(elem);
    return elem != dblist;
}
