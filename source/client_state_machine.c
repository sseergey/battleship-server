#include "client_state_machine.h"
#include "server.h"
#include "network.h"
#include <assert.h>
#include <ctype.h>

void client(const char* buf, struct data_t* data, const struct event_t* recv_event) {
    // Собрать сообщение
    char recv_msg[MESSAGE_MAX_SIZE];
    memset(recv_msg, 0, sizeof(recv_msg));
    if (!assembleString(recv_msg, data, buf)) {
        return;
    }
    printf("CLIENT=%d;STATE=%d\n", data->fd, data->state);
    switch (data->state) {
    case HANDSHAKE:
        data->state = handshake(recv_msg, data->fd);
        break;

    case ACQ_RECV:
        data->state = acqRecv(recv_msg, data->fd, data);
        break;

    case CHOOSE_RIVAL_RECV:
        data->state = chooseRivalRecv(recv_msg, data);
        break;

    case WAIT_GAME_OFFER:
        data->state = waitGameOffer(recv_msg, data, recv_event);
        break;

    case WAIT_GAME_STATUS:
        data->state = waitGameStatus(recv_msg, data->fd);
        break;

    case WAIT_SV_EVENT:
        data->state = waitSvEvent(recv_msg, data->fd);
        break;

    case PLACE_SHIPS:
        data->state = placeShip(recv_msg, data->fd);
        break;

    case GAME_ACTION:
        data->state = gameAction(recv_msg, data->fd);
        break;

    case CLOSED:
        closed();
        break;
    }
}


int handshake(const char* recv_msg, int fd) {

    printf("%s", recv_msg);

    if (!strcmp("battle ship client 1\n", recv_msg)) {
        server(fd, "name?\n");
        return ACQ_RECV;
    }
    kill_client(fd);
    return CLOSED;
}


int acqRecv(const char* recv_msg, int fd, struct data_t* data) {

    const char* name = recv_msg;
    if (!strcmp(name, "\n") || !strcmp(name, "*\n") || !strcmp(name, "*list\n")) {
        server(fd, "name: rejected\n");
        kill_client(fd);
        return CLOSED;
    }
    else if (nameIsOk(fd, name)) {
        strcpy(data->name, recv_msg);

        server(fd, "name: accepted\n");
        put_to_replace_list(fd, recv_msg);

        sendListMSG(fd);
        server(fd, "enemy?\n");
        return CHOOSE_RIVAL_RECV;
    }
    else {
        server(fd, "name: rejected\n");
        kill_client(fd);
        return CLOSED;
    }
}


int chooseRivalRecv(const char* recv_msg, struct data_t* data) {
    if (!strcmp(recv_msg, "\n")) {
        kill_client(data->fd);
        return CLOSED;
    }

    int fd = data->fd;
    const char* myName = data->name;

    if (!strcmp(recv_msg, "*\n")) {
        server(fd, "enemy: ok\n");
        strcpy(data->enemy_name, recv_msg);
        data->enemy_fd = -2;

        put_to_event_queue(gen_event_WANT(fd));
        return WAIT_GAME_OFFER;
    }
    else if (!strcmp(recv_msg, "*list\n")) {
        sendListMSG(fd);
        return CHOOSE_RIVAL_RECV;
    }
    else if (strcmp(myName, recv_msg)) {
        server(fd, "enemy: ok\n");

        strcpy(data->enemy_name, recv_msg);
        data->enemy_fd = -1;

        const struct data_t* item = (const struct data_t* )first_dblist_element(client_list);
        const struct data_t* start = (const struct data_t* )client_list;
        while(item != start) {
            if (!strcmp(item->name, recv_msg)) {
                data->enemy_fd = item->fd;
                break;
            }
            item = (const struct data_t* )(((const struct dblist_t* )item)->next);
        }

        put_to_event_queue(gen_event_WANT(fd));
        return WAIT_GAME_OFFER;
    }
    else {
        server(fd, "enemy: rejected\n");
        kill_client(fd);
        return CLOSED;
    }
}


int waitGameOffer(const char* recv_msg, struct data_t* data, const struct event_t* recv_event) {
    printf("CLIENT=%d; fromFD=%d; DATA=%s", data->fd, recv_event->fromFd, recv_msg);
    if (strcmp(recv_msg, "create\n") || recv_event->fromFd != 0) {
        kill_client(data->fd);
        return CLOSED;
    }

    char msg[MESSAGE_MAX_SIZE] = "game: ";
    const char* opp_name = get_from_client_list(client_list, recv_event->enemyFd)->name;
    strcat(msg, opp_name);
    server(data->fd, msg);

    return WAIT_GAME_STATUS;
}


int waitGameStatus(const char* recv, int fd) {
    int cnt = 0;

    printf("recv= %s\n", recv);
    if (strlen(recv) == 2 && recv[1] == '\n') {
        if (toupper(recv[0]) == 'Y') {
            cnt = 1;
        }
        else if (toupper(recv[0]) == 'N') {
            cnt = -1;
        } else {
            kill_client(fd);
            return CLOSED;
        }
    }
    else {
        kill_client(fd);
        return CLOSED;
    }


    if (cnt == -1) {
        // gen_event(no)
        put_to_event_queue(gen_event_DECLINED(fd));
//        put_to_event_queue(gen_event_WANT(fd));

        return WAIT_SV_EVENT;
    }
    else if (cnt == 1) {
        // gen_event(yes)
        put_to_event_queue(gen_event_ACCEPT(fd));

        return WAIT_SV_EVENT;
    }
    else {
        kill_client(fd);
        return CLOSED;
    }
}


int waitSvEvent(const char* recv, int fd) {

    printf("recv= %s\n", recv);
    if (!strcmp(recv, "invite: accepted\n")) {
        server(fd, recv);
    }
    else if (!strcmp(recv, "invite: rejected\n")) {
        server(fd, recv);
    }
    else {
        kill_client(fd);
        return CLOSED;
    }

    const struct game_t* game = get_from_game_list(game_list, fd);
    if (game != NULL) {
        if (game->game_started) {
            server(fd, "place 4-ship?\n");
            return PLACE_SHIPS;
        }
        else {
            remove_from_game_list(game_list, fd);
            gen_event_WANT(fd);
            return WAIT_GAME_OFFER;
        }
    }
    else {
        gen_event_WANT(fd);
        return WAIT_GAME_OFFER;
    }
}


int placeShip(const char* recv_msg, int fd) {

    if (strlen(recv_msg) != 5 || (recv_msg[0] != 'h' && recv_msg[0] != 'v')
            || !(isInBoardRange(recv_msg[2]-'A'))
            || !(isInBoardRange(recv_msg[3]-'0'))) {
        kill_client(fd);
        return CLOSED;
    }
    char d = recv_msg[0];
    int startPosX = recv_msg[2]-'A';
    int startPosY = recv_msg[3]-'0';

    struct game_t* game = get_from_game_list(game_list, fd);
    assert(game != NULL);

    char msg[MESSAGE_MAX_SIZE] = "place ";

    struct player_t* me = NULL;
    const struct player_t* opp = NULL;
    if (fd == game->player1.fd) {
        me = &game->player1;
        opp = &game->player2;
    } else {
        me = &game->player2;
        opp = &game->player1;
    }

    int length = game->ship[me->current_ship-1];
    if (isShipOk(startPosX, startPosY, d, length, fd)) {
        for (int i = 0; i < length; i++)
            if (d == 'h')
                me->field[startPosY][startPosX+i] = 'O';
            else
                me->field[startPosY+i][startPosX] = 'O';
    }
    else {
        printf("recv= %s\n", recv_msg);
        kill_client(fd);
        return CLOSED;
    }

    if (me->req || opp->req) {
        return GAME_ACTION;
    }

    if (me->current_ship == 10) {
        if (opp->current_ship == 10) {
            server(me->fd, "go\n");
            server(opp->fd, "go\n");
            server(me->fd, "fire?\n");
            me->req = true;
        }


        return GAME_ACTION;
    }

    char ch =(char)(game->ship[me->current_ship++] + '0');
    msg[strlen(msg)] = ch;

    msg[strlen(msg)] = '\0';
    strcat(msg, "-ship?\n");

    server(fd, msg);

    return PLACE_SHIPS;
}


int gameAction(const char* recv_msg, int fd) {
    printf("%s\n", recv_msg);

    const struct game_t* game = get_from_game_list(game_list, fd);
    assert(game != NULL);

    if (!strcmp(recv_msg, "exit\n")) {
        if (game->game_started)
            return exitMSG(fd);
        else {
            gen_event_WANT(fd);
            return WAIT_GAME_OFFER;
        }
    }
    else if (!strcmp(recv_msg, "field\n")) {
        if (game->game_started) {
            sendField(fd);
            return GAME_ACTION;
        }
        else {
            gen_event_WANT(fd);
            return WAIT_GAME_OFFER;
        }
    }
    else if (strlen(recv_msg) == 3) {
        if (game->game_started) {
            if (isInBoardRange(recv_msg[0]-'A') && isInBoardRange(recv_msg[1]-'0')) {
                return fireMSG(fd, recv_msg);
            }
            else {
                goto err;
            }
        }
        else {
            if (isInBoardRange(recv_msg[0]-'A') && isInBoardRange(recv_msg[1]-'0')) {
                gen_event_WANT(fd);
                return WAIT_GAME_OFFER;
            }
            else {
                goto err;
            }
        }
    } else {
        goto err;
    }

err:
    return exitMSG(fd);
}


int closed(void) {
    return CLOSED;
}


bool assembleString(char* recv, struct data_t* data, const char* buf) {

    size_t strL1 = strlen(data->buf);
    size_t strL2 = strlen(buf);

    for (size_t i = strL1; i < strL1 + strL2; i++) {
        data->buf[i] = buf[i - strL1];
    }

    bool ok = false;
    if (data->buf[strL1+strL2-1] == '\n') {
        for (size_t i = 0; i < strL1 + strL2; i++) {
            recv[i] = data->buf[i];
        }
        for (size_t i = strL1 + strL2; i < strlen(data->buf); i++) {
            data->buf[i - strL1 - strL2] = data->buf[i];
        }
        size_t len = strlen(data->buf);
        for (size_t i = len-strL1-strL2; i < len; i++) {
            data->buf[i] = '\0';
        }
        ok = true;
    }

    return ok;
}


bool isShipHere(char point) {
    return point == 'O' || point == 'X';
}

bool isInBoardRange(int x) {
    return 0 <= x && x < 10;
}


int attack(int x, int y, int fd) {

    const struct game_t* game = get_from_game_list(game_list, fd);
    assert(game != NULL);

    const char (*field)[10] = NULL;

    if (fd == game->player1.fd) {
        field = game->player2.field;
    } else {
        field = game->player1.field;
    }

    if (field[y][x] == 'O') {

        bool horizontal = false;
        if (isInBoardRange(x+1) && isShipHere(field[y][x+1])) {
            horizontal = true;
        }
        if (isInBoardRange(x-1) && isShipHere(field[y][x-1])) {
            horizontal = true;
        }

        bool killed = true;
        if (horizontal) {
            int pos = -1;
            while(isInBoardRange(x+pos) && isShipHere(field[y][pos+x])) {
                if (field[y][pos+x] == 'O') {
                    killed = false;
                }
                pos--;
            }
            pos = 1;
            while(isInBoardRange(x+pos) && isShipHere(field[y][pos+x])) {
                if (field[y][pos+x] == 'O') {
                    killed = false;
                }
                pos--;
            }
        } else {
            int pos = -1;
            while(isInBoardRange(y+pos) && isShipHere(field[pos+y][x])) {
                if (field[pos+y][x] == 'O') {
                    killed = false;
                }
                pos--;
            }
            pos = 1;
            while(isInBoardRange(y+pos) && isShipHere(field[pos+y][x])) {
                if (field[pos+y][x] == 'O') {
                    killed = false;
                }
                pos--;
            }
        }

        if (killed)
            return 1;
        else
            return 2;
    }
    else
        return 3;
}


bool nameIsOk(int fd, const char* name) {
    struct data_t* item = (struct data_t* )first_dblist_element(client_list);
    struct data_t* start = (struct data_t* )client_list;

    while(item != start) {
        if (item->fd != fd) {
            if (!strcmp(item->name, name)) {
                return false;
            }
        }
        item = (struct data_t* )(((struct dblist_t* )item)->next);
    }
    return true;
}


bool isShipOk(int x, int y, char dir, int length, int fd) {
    const struct game_t* game = get_from_game_list(game_list, fd);
    assert(game != NULL);

    const struct player_t* player = NULL;
    if (fd == game->player1.fd) {
        player = &game->player1;
    } else {
        player = &game->player2;
    }


    if (dir == 'h') {

        for (int i = x; i < x+length; i++) {
            bool flag = false;

            for (int j = -1; j < 1; j++) {
                for (int k = -1; k < 1; k++) {
                    if (isInBoardRange(y+k) && isInBoardRange(i+j)  && player->field[y+k][i+j] == 'O') {
                        flag = true;
                        break;
                    }
                }
            }
            if (flag) {
                return false;
            }
        }

    } else {

        for (int i = y; i < y+length; i++) {
            bool flag = false;

            for (int j = -1; j < 1; j++) {
                for (int k = -1; k < 1; k++) {
                    if (isInBoardRange(i+k) && isInBoardRange(x+j) && player->field[i+k][x+j] == 'O') {
                        flag = true;
                        break;
                    }
                }
            }
            if (flag) {
                return false;
            }
        }

    }

    return true;
}


void kill_client(int fd) {
    put_to_kill_list(fd);
}


enum fire_results{
    KILL = 1,
    HIT = 2,
    MISS,
};


int fireMSG(int fd, const char* recv_msg) {
    char message[MESSAGE_BUFFER_SIZE];
    struct game_t* game = get_from_game_list(game_list, fd);
    assert(game != NULL);

    int posX = recv_msg[0]-'A';
    int posY = recv_msg[1]-'0';
    int result = attack(posX, posY, fd);

    printf("RESULT =%d;%d\n", result, fd);

    struct player_t* me = NULL;
    struct player_t* opp = NULL;
    if (fd == game->player1.fd) {
        me = &game->player1;
        opp = &game->player2;
    } else {
        me = &game->player2;
        opp = &game->player1;
    }

    switch (result) {
        case KILL:
            me->enemyField[posY][posX] = 'X';
            opp->field[posY][posX] = 'X';
            me->killedDecks++;

            sprintf(message, "fire: you %c%c, kill\n",
                    (char)(posX+(int)'A'), (char)(posY+(int)'0'));
            server(me->fd, message);
            memset(message, 0, strlen(message));
            sprintf(message, "fire: enemy %c%c, kill\n",
                    (char)(posX+(int)'A'), (char)(posY+(int)'0'));
            server(opp->fd, message);
            if (me->killedDecks == 10) {
                server(me->fd, "end: win\n");
                server(opp->fd, "end: loss\n");
                return ACQ_RECV;
            }
            server(me->fd, "fire?\n");

            return GAME_ACTION;
        case HIT:
            me->enemyField[posY][posX] = 'X';
            opp->field[posY][posX] = 'X';

            sprintf(message, "fire: you %c%c, hit\n",
                    (char)(posX+(int)'A'), (char)(posY+(int)'0'));
            server(me->fd, message);
            memset(message, 0, strlen(message));
            sprintf(message, "fire: enemy %c%c, hit\n",
                    (char)(posX+(int)'A'), (char)(posY+(int)'0'));
            server(opp->fd, message);
            server(me->fd, "fire?\n");

            return GAME_ACTION;
        case MISS:
            me->enemyField[posY][posX] = '.';
            opp->field[posY][posX] = '.';

            sprintf(message, "fire: you %c%c, miss\n",
                    (char)(posX+(int)'A'), (char)(posY+(int)'0'));

            server(me->fd, message);
            memset(message, 0, strlen(message));
            sprintf(message, "fire: enemy %c%c, miss\n",
                    (char)(posX+(int)'A'), (char)(posY+(int)'0'));
            server(opp->fd, message);
            server(opp->fd, "fire?\n");

            return GAME_ACTION;
    }

    return exitMSG(fd);
}


int exitMSG(int fd) {
    struct game_t* game = get_from_game_list(game_list, fd);
    assert(game != NULL);

    struct player_t *player = NULL;
    if (fd == game->player1.fd) {
        player = &game->player2;
    } else {
        player = &game->player1;
    }

    server(fd, "end: techlose\n");
    server(player->fd, "end: techwin\n");
    game->game_started = false;

    kill_client(fd);
    return CLOSED;
}


struct event_t* gen_event_ACCEPT(int fd) {
    struct event_t* event = (struct event_t* )malloc(sizeof(struct event_t));
    memset(event->data, 0, sizeof(event->data));
    strcpy(event->data, "agree\n");

    event->fd = 0;
    event->fromFd = fd;
    event->agree = true;

    return event;
}


struct event_t* gen_event_DECLINED(int fd) {
    struct event_t* event = (struct event_t* )malloc(sizeof(struct event_t));
    memset(event->data, 0, sizeof(event->data));
    strcpy(event->data, "agree\n");

    event->fd = 0;
    event->fromFd = fd;
    event->agree = false;

    return event;
}


struct event_t* gen_event_WANT(int fd) {
    struct event_t* event = (struct event_t* )malloc(sizeof(struct event_t));
    memset(event->data, 0, sizeof(event->data));
    event->fd = 0;
    event->fromFd = fd;
    strcpy(event->data, "want\n");

    return event;
}
