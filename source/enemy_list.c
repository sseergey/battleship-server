#include "enemy_list.h"
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

struct dblist_t* init_enemy_list(void) {
    struct dblist_t* list = (struct dblist_t* )malloc(sizeof(struct enemy_t));
    init_dblist(list);
    return list;
}


void release_enemy_list(struct dblist_t* list) {
    assert(list);
    release_dblist(list);
    free(list);
}


void put_to_enemy_list(struct dblist_t* list, struct enemy_t* item) {
    assert(list);
    assert(item);
    struct dblist_t* last_item = last_dblist_element(list);
    add_to_dblist(last_item, (struct dblist_t* )item);
}


struct enemy_t* get_from_enemy_list(struct dblist_t* list, int fd) {
    assert(list);
    struct enemy_t* item = (struct enemy_t* )first_dblist_element(list);
    struct enemy_t* start = (struct enemy_t* )list;

    while(item != start) {
        if (item->fd == fd) {
            return item;
        }
        item = (struct enemy_t* )(((struct dblist_t* )item)->next);
    }

    return NULL;
}


void remove_from_enemy_list(struct dblist_t* list, int fd) {
    assert(list);
    struct enemy_t* item = get_from_enemy_list(list, fd);
    if (item != NULL)
        remove_from_dblist((struct dblist_t* )item);
}
