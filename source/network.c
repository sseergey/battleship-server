#include "network.h"
#include "server.h"
#include <assert.h>

ssize_t server(int fd, const char* data) {
    char msg[MESSAGE_BUFFER_SIZE];
    memset(msg, 0, MESSAGE_BUFFER_SIZE);
    snprintf(msg, MESSAGE_BUFFER_SIZE, "%s", data);
    printf("%s", msg);

    return send(fd, msg, strlen(msg), 0);
}

int sendListMSG(int fd) { // N - кол-во игроков жел сыграть с вами

    struct enemy_t* item = (struct enemy_t* )first_dblist_element(waiting_players_list);
    struct enemy_t* start = (struct enemy_t* )waiting_players_list;

    int cnt = 0;
    while(item != start) {
        if (!strcmp(get_from_client_list(client_list, item->fd)->enemy_name, get_from_client_list(client_list, fd)->name)) {
            cnt++;
        }
        item = (struct enemy_t* )(((struct dblist_t* )item)->next);
    }
    char msg[MESSAGE_MAX_SIZE];
    memset(msg, 0, sizeof(msg));

    sprintf(msg, "list %d:\n", cnt);
    server(fd, msg);

    item = (struct enemy_t* )first_dblist_element(waiting_players_list);
    start = (struct enemy_t* )waiting_players_list;
    while(item != start) {
        if (item->fd != fd) {
            if (!strcmp(get_from_client_list(client_list, item->fd)->enemy_name, get_from_client_list(client_list, fd)->name)) {
                char enemy_msg[MESSAGE_MAX_SIZE];
                sprintf(enemy_msg, "*%s", item->name);
                server(fd, enemy_msg);
            }
        }
        item = (struct enemy_t* )(((struct dblist_t* )item)->next);
    }

    return 0;
}


int sendField(int fd) {
    struct game_t* game = get_from_game_list(game_list, fd);
    assert(game != NULL);

    struct player_t* player = NULL;
    if (game->player1.fd == fd) {
        player = &game->player1;
    }
    else {
        player = &game->player2;
    }

    char msg[512] =
            "field:\n"
            "your           enemy\n"
            "#|ABCDEFGHIJ|  #|ABCDEFGHIJ|\n"
            "-+----------|  -+----------|\n"
            "%|%%%%%%%%%%|  %|%%%%%%%%%%|\n"
            "%|%%%%%%%%%%|  %|%%%%%%%%%%|\n"
            "%|%%%%%%%%%%|  %|%%%%%%%%%%|\n"
            "%|%%%%%%%%%%|  %|%%%%%%%%%%|\n"
            "%|%%%%%%%%%%|  %|%%%%%%%%%%|\n"
            "%|%%%%%%%%%%|  %|%%%%%%%%%%|\n"
            "%|%%%%%%%%%%|  %|%%%%%%%%%%|\n"
            "%|%%%%%%%%%%|  %|%%%%%%%%%%|\n"
            "%|%%%%%%%%%%|  %|%%%%%%%%%%|\n"
            "%|%%%%%%%%%%|  %|%%%%%%%%%%|\n"
            "-+----------+  -+----------+\n";
    char *p = msg;
    for (int i = 0; i < 10; i++) {
        p = strchr(p, '%');
        *p = (char)('0' + i);
        for(int j = 0; j < 10; j++) {
            p = strchr(p, '%');
            *p = player->field[i][j];
        }

        p = strchr(p, '%');
        *p = (char)('0' + i);
        for(int j = 0; j < 10; j++) {
            p = strchr(p, '%');
            *p = player->enemyField[i][j];
        }
    }
    server(fd, msg);

    return 0;
}
