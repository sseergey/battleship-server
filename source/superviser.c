#include "superviser.h"
#include "server.h"
#include "client_state_machine.h"
#include <assert.h>

void supervisor(const struct event_t* event) {

    if (event->fd == 0) {
        if (!strcmp(event->data, "want\n"))
            want_supervisor(event);

        if (!strcmp(event->data, "agree\n"))
            agree_supervisor(event);

//        printf("Work with UnexpectableEvent\n");

    }
    else {
//        printf("Work with event for CLIENT\n");
        client_supervisor(event);
    }
}


void client_supervisor(const struct event_t* event) {
    client(event->data, get_from_client_list(client_list, event->fd), event);
}


bool checkForNameGlobbing(const struct data_t* data, const struct data_t* wait_data) {
    assert(data);
    assert(wait_data);
    if (data->enemy_fd == -1)
        return false;
    if (!strcmp(data->name, wait_data->enemy_name) && !strcmp(data->enemy_name, wait_data->name))
        return true;
    if (!strcmp(data->enemy_name, "*\n") && !strcmp(data->name, wait_data->enemy_name))
        return true;
    if (!strcmp(data->enemy_name, wait_data->name) && !strcmp(wait_data->enemy_name, "*\n"))
        return true;
    if (!strcmp(data->enemy_name, "*\n") && !strcmp(wait_data->enemy_name, "*\n"))
        return true;
    return false;
}


void want_supervisor(const struct event_t* event) {
    struct enemy_t* player = (struct enemy_t* )malloc(sizeof(struct enemy_t));
    player->fd = event->fromFd;

    const struct data_t* data = get_from_client_list(client_list, event->fromFd);
    strcpy(player->name, data->name);

    struct enemy_t* wait_item = (struct enemy_t* )first_dblist_element(waiting_players_list);
    struct enemy_t* wait_start = (struct enemy_t* )waiting_players_list;

    while(wait_item != wait_start) {
        const struct data_t* wait_data = get_from_client_list(client_list, wait_item->fd);
        if (checkForNameGlobbing(data, wait_data)) {
            struct event_t* event1 = (struct event_t* )malloc(sizeof(struct event_t));
            struct event_t* event2 = (struct event_t* )malloc(sizeof(struct event_t));

            memset(event1, 0, sizeof(struct event_t));
            memset(event2, 0, sizeof(struct event_t));

            strcpy(event1->data, "create\n");
            strcpy(event2->data, "create\n");

            event1->fd = player->fd;
            event1->enemyFd = wait_data->fd;
            event1->fromFd = 0;

            event2->fd = wait_data->fd;
            event2->enemyFd = player->fd;
            event2->fromFd = 0;

            put_to_event_queue(event1);
            put_to_event_queue(event2);

            struct game_t* game = (struct game_t* )malloc(sizeof(struct game_t));
            game->player1.fd = player->fd;
            game->player2.fd = wait_data->fd;
            game->player1.agree = false;
            game->player2.agree = false;
            game->player1.req = false;
            game->player2.req = false;

            memset(game->player1.field, ' ', sizeof(game->player1.field));
            memset(game->player2.field, ' ', sizeof(game->player2.field));
            memset(game->player1.enemyField, ' ', sizeof(game->player1.enemyField));
            memset(game->player2.enemyField, ' ', sizeof(game->player2.enemyField));

            put_to_game_list(game_list, game);
            break;
        }

        wait_item = (struct enemy_t* )(((struct dblist_t* )wait_item)->next);
    }
    put_to_enemy_list(waiting_players_list, player);
}


void gen_event_invite(int fd, const char *data) {
    struct event_t* event = (struct event_t* )malloc(sizeof(struct event_t));
    event->fd = fd;
    strcpy(event->data, data);
    put_to_event_queue(event);
}


void agree_supervisor(const struct event_t* event) {
    struct game_t* game = get_from_game_list(game_list, event->fromFd);
    assert(game != NULL);

    if (game->player1.fd == event->fromFd) {
        game->player1.req = true;
        game->player1.agree = event->agree;
    }
    else {
        game->player2.req = true;
        game->player2.agree = event->agree;
    }

    if (game->player1.req && game->player2.req) {
        if (game->player1.agree && game->player2.agree) {
            int ship[] = {4, 3, 3, 2, 2, 2, 1, 1, 1, 1};
            gen_event_invite(game->player1.fd, "invite: accepted\n");
            gen_event_invite(game->player2.fd, "invite: accepted\n");
            for (int i = 0 ; i< 10; i++) {
                game->ship[i] = ship[i];
            }
            game->player1.current_ship = 1;
            game->player2.current_ship = 1;
            game->game_started = true;
            game->player1.req = false;
            game->player2.req = false;
        }
        else if (game->player1.agree && !game->player2.agree) {
            gen_event_invite(game->player1.fd, "invite: rejected\n");
            gen_event_invite(game->player2.fd, "invite: accepted\n");
            game->game_started = false;
        }
        else if (!game->player1.agree && game->player2.agree) {
            gen_event_invite(game->player1.fd, "invite: accepted\n");
            gen_event_invite(game->player2.fd, "invite: rejected\n");
            game->game_started = false;
        }
        else if (!game->player1.agree && !game->player2.agree) {
            gen_event_invite(game->player1.fd, "invite: rejected\n");
            gen_event_invite(game->player2.fd, "invite: rejected\n");
            game->game_started = false;
        }
    }
}
