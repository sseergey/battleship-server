#include "event_queue.h"
#include <stdlib.h>
#include <stddef.h>
#include <assert.h>

struct dblist_t* init_queue(void) {
    struct dblist_t* queue = (struct dblist_t* )malloc(sizeof(struct dblist_t));
    init_dblist(queue);
    return queue;
}

void release_queue(struct dblist_t* queue) {
    assert(queue);
    release_dblist(queue);
    free(queue);
}

void put_to_queue(struct dblist_t* queue, struct event_t* item) {
    assert(queue);
    assert(item);
    struct dblist_t* last_item = last_dblist_element(queue);
    add_to_dblist(last_item, (struct dblist_t* )item);
}

struct event_t* get_from_queue(struct dblist_t* queue) {
    assert(queue);
    struct dblist_t* item = NULL;

    if (!is_dblist_empty(queue)) {
        item = first_dblist_element(queue);
        remove_from_dblist(item);
    }

    return (struct event_t* )item;
}
